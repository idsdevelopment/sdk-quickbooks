// Copyright (c) 2001-2003 Quadralay Corporation.  All rights reserved.
//

function  WWHBookGroups_Books(ParamTop)
{


  ParamTop.fAddDirectory("Release Notes", null, null, null, null);
  ParamTop.fAddDirectory("Technical Overview", null, null, null, null);
  ParamTop.fAddDirectory("QBSDK Programmers Guide", null, null, null, null);
  ParamTop.fAddDirectory("QBWC Developers Guide", null, null, null, null);
}

function  WWHBookGroups_ShowBooks()
{
  return true;
}

function  WWHBookGroups_ExpandAllAtTop()
{
  return false;
}
