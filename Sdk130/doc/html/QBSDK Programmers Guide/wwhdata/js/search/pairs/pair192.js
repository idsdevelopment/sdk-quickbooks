function FileData_Pairs(x)
{
x.t("example","illustrates");
x.t("example","suppose");
x.t("$0.39.","adding");
x.t("shown","invoice.");
x.t("create","sales");
x.t("point.","example");
x.t("$0.3866","rounds");
x.t("state","tax");
x.t("doesn\u2019t","necessarily");
x.t("amount.","number");
x.t("however","invoice");
x.t("however","using");
x.t("total","tax");
x.t("total","$38.66.");
x.t("down","$1.93.");
x.t("items","total");
x.t("items","add");
x.t("$1.55.","county");
x.t("calculated.","following");
x.t("tax","create");
x.t("tax","doesn\u2019t");
x.t("tax","items");
x.t("tax","group");
x.t("tax","percentages.");
x.t("tax","sure");
x.t("tax","$1.9330");
x.t("tax","sales");
x.t("tax","calculating");
x.t("tax","$1.94");
x.t("tax","percent.");
x.t("tax","amount");
x.t("tax","value");
x.t("tax","taxes");
x.t("tax","percentage");
x.t("tax","percent");
x.t("group","tax");
x.t("group","sales");
x.t("group","individually");
x.t("group","include");
x.t("group","used");
x.t("add","group.");
x.t("sum","tax");
x.t("sum","individual");
x.t("taxable","items");
x.t("expect","tax");
x.t("assume","invoice\u2019s");
x.t("results","obtain");
x.t("percentages.","however");
x.t("sure","calculate");
x.t("invoice","displayed");
x.t("invoice","actually");
x.t("obtain","correct");
x.t("$1.9330","rounded");
x.t("taxes.","sales");
x.t("sales","tax");
x.t("discounts","versions");
x.t("illustrates","point.");
x.t("county","tax");
x.t("invoice\u2019s","taxable");
x.t("calculate","sales");
x.t("calculate","individual");
x.t("calculating","sales");
x.t("occurs","individual");
x.t("suppose","state");
x.t("$1.94","amount");
x.t("$1.94","correct");
x.t("number","correct");
x.t("group.","sales");
x.t("$38.66.","percent");
x.t("necessarily","reflect");
x.t("individually","sum");
x.t("$1.93.","however");
x.t("you\u2019d","expect");
x.t("together","total");
x.t("displayed","sum");
x.t("displayed","percent.");
x.t("percent.","create");
x.t("percent.","assume");
x.t("amount","shown");
x.t("amount","rounding");
x.t("include","taxes.");
x.t("reflect","correct");
x.t("value","you\u2019d");
x.t("$1.5464","rounds");
x.t("consequently","application");
x.t("actually","show");
x.t("computes","sales");
x.t("show","$1.94");
x.t("correct","state");
x.t("correct","amount.");
x.t("correct","total");
x.t("correct","total.");
x.t("application","computes");
x.t("taxes","calculated.");
x.t("taxes","discounts");
x.t("taxes","together");
x.t("transaction","tax");
x.t("percentage","calculate");
x.t("percentage","displayed");
x.t("two","taxes");
x.t("invoice.","consequently");
x.t("used","invoice");
x.t("used","transaction");
x.t("individual","tax");
x.t("individual","results");
x.t("individual","taxes");
x.t("following","example");
x.t("percent","$0.3866");
x.t("percent","tax");
x.t("percent","county");
x.t("percent","$1.5464");
x.t("rounded","down");
x.t("using","group");
x.t("rounding","occurs");
x.t("adding","two");
x.t("versions","calculating");
x.t("rounds","$0.39.");
x.t("rounds","$1.55.");
}
