function FileData_Pairs(x)
{
x.t("qbfc","automatically");
x.t("qbfc","language");
x.t("closeconnection","closes");
x.t("closeconnection","code");
x.t("closeconnection","wasn\u2019t");
x.t("provided","beginsession");
x.t("sample","provided");
x.t("sample","sample");
x.t("explicitly","before.");
x.t("calls","endsession");
x.t("connection","quickbooks.");
x.t("automatically","call");
x.t("beginsession","code");
x.t("qbsessionmanager.closeconnection","qbfc");
x.t("qbsessionmanager.closeconnection","qbsessionmanager.closeconnection");
x.t("qbsessionmanager.closeconnection","hresult");
x.t("endsession","necessary.");
x.t("hresult","closeconnection");
x.t("quickbooks.","qbsessionmanager");
x.t("language","reference");
x.t("closes","connection");
x.t("scope","qbfc");
x.t("reference","qbsessionmanager.closeconnection");
x.t("code","sample");
x.t("code","sample.");
x.t("wasn\u2019t","called");
x.t("called","explicitly");
x.t("qbsessionmanager","object");
x.t("call","closeconnection");
x.t("necessary.","closeconnection");
x.t("before.","calls");
x.t("object","scope");
}
