function FileData_Pairs(x)
{
x.t("qbfc","language");
x.t("example","array");
x.t("example","release");
x.t("2007","u.s.");
x.t("version","numbers");
x.t("5.0","6.0");
x.t("currently","open");
x.t("freeing","memory");
x.t("description","qbxmlversionsforsession");
x.t("responsible","freeing");
x.t("(bstr)","ppsa");
x.t("program","application");
x.t("retval","safearray");
x.t("open","connection");
x.t("sample","code");
x.t("hostquery","request");
x.t("hostquery","returns");
x.t("connection","usually");
x.t("u.s.","edition");
x.t("safearraydestroy","(ppsa)");
x.t("usually","information");
x.t("require.","parameters");
x.t("canadian","edition");
x.t("indicates","versions");
x.t("list","qbxml");
x.t("complete","list");
x.t("3.0","4.0");
x.t("(ppsa)","sample");
x.t("processor","supports.");
x.t("processor","quickbooks");
x.t("processor","2003");
x.t("array.","example");
x.t("manual","hostquery");
x.t("parameters","ppsa");
x.t("hresult","qbxmlversionsforsession");
x.t("array","qbxml");
x.t("array","using");
x.t("array","contains");
x.t("6.0","application");
x.t("request","processor");
x.t("request","described");
x.t("supported","currently");
x.t("supported","quickbooks");
x.t("1.0","1.1");
x.t("ppsa","indicates");
x.t("ppsa","array.");
x.t("ppsa","array");
x.t("qbxml","version");
x.t("qbxml","specification");
x.t("qbxml","versions");
x.t("1.1","2.0");
x.t("quickbooks.","usage");
x.t("different","information");
x.t("chapter","making");
x.t("language","reference");
x.t("language","call");
x.t("qbxmlversionsforsession","retval");
x.t("qbxmlversionsforsession","chapter");
x.t("returned","hostquery");
x.t("release","memory");
x.t("session.","note");
x.t("reference","qbsessionmanager.qbxmlversionsforsession");
x.t("connected","session.");
x.t("code","description");
x.t("ca2.0","application");
x.t("memory","array");
x.t("memory","used");
x.t("4.0","5.0");
x.t("usage","application");
x.t("supports.","example");
x.t("making","application");
x.t("qbsessionmanager.qbxmlversionsforsession","qbfc");
x.t("qbsessionmanager.qbxmlversionsforsession","hresult");
x.t("qbsessionmanager.qbxmlversionsforsession","qbsessionmanager.qbxmlversionsforsession");
x.t("information","different");
x.t("information","returned");
x.t("information","application");
x.t("application","responsible");
x.t("application","robust.");
x.t("application","require.");
x.t("application","connected");
x.t("application","using");
x.t("specification","supported");
x.t("described","concepts");
x.t("note","information");
x.t("quickbooks","2007");
x.t("quickbooks","program");
x.t("quickbooks","request");
x.t("numbers","quickbooks");
x.t("2.0","2.1");
x.t("edition","quickbooks.");
x.t("edition","contains");
x.t("call","safearraydestroy");
x.t("2.1","3.0");
x.t("used","ppsa");
x.t("safearray","(bstr)");
x.t("using","request");
x.t("using","language");
x.t("returns","complete");
x.t("versions","supported");
x.t("versions","qbxml");
x.t("2003","canadian");
x.t("concepts","manual");
x.t("contains","1.0");
x.t("contains","ca2.0");
}
