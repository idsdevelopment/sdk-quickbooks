function FileData_Pairs(x)
{
x.t("example","payment");
x.t("excellent","overpayment");
x.t("let\u2019s","look");
x.t("discount","credits:");
x.t("available","message");
x.t("just","create");
x.t("just","credit");
x.t("just","add");
x.t("just","cleaned");
x.t("just","click");
x.t("just","wasn\u2019t");
x.t("right","thing");
x.t("right","answer.");
x.t("balance","renee\u2019s");
x.t("customer:job","renee");
x.t("nothing","notice");
x.t("nothing","actually");
x.t("lists","really");
x.t("create","credit");
x.t("create","check");
x.t("business","logic.");
x.t("doing","give");
x.t("apply","payment");
x.t("apply","overpayment");
x.t("try","correct");
x.t("credit","double");
x.t("credit","original");
x.t("credit","refunded");
x.t("credit","memo");
x.t("credit","already");
x.t("credit","refund");
x.t("double","problem.");
x.t("user","interface");
x.t("expense","line");
x.t("line","account");
x.t("line","payment");
x.t("there\u2019s","patch");
x.t("confusion","clearing");
x.t("credits:","credit");
x.t("quirk","quickbooks");
x.t("deposited","accountants");
x.t("deposited","can\u2019t");
x.t("look","payment:");
x.t("interface","really");
x.t("thing","balance");
x.t("doesn\u2019t","update");
x.t("customer","credits");
x.t("close","transaction");
x.t("delete","overpayment");
x.t("importantly","extra");
x.t("castle","construction");
x.t("however","doesn\u2019t");
x.t("that\u2019s","right");
x.t("checks","window.");
x.t("original","payment");
x.t("original","transaction");
x.t("really","just");
x.t("really","want");
x.t("really","payment");
x.t("want","apply");
x.t("start","creating");
x.t("new","payment");
x.t("gone.","even");
x.t("payment:","quickbooks");
x.t("again","credit");
x.t("add","check");
x.t("refunded","cleaned");
x.t("check","line");
x.t("check","original");
x.t("check","click");
x.t("check","wrote");
x.t("check","transaction");
x.t("check","mark");
x.t("check","renee");
x.t("receipt.","there\u2019s");
x.t("applies","nothing");
x.t("receipt","renee:");
x.t("receipt","payment");
x.t("receipt","won\u2019t");
x.t("receipt","have.");
x.t("receipt","created");
x.t("account","return");
x.t("account","accounts");
x.t("creating","new");
x.t("renee:","note");
x.t("manual","solution");
x.t("$200","credit");
x.t("receivable","customer:job");
x.t("receivable","expense.");
x.t("expense.","want");
x.t("construction","refund");
x.t("problem.","that\u2019s");
x.t("problem.","instead");
x.t("problem.","guess");
x.t("up:","excellent");
x.t("discounts","credits.");
x.t("modification","payment");
x.t("write","checks");
x.t("earlier","new");
x.t("earlier","accounts");
x.t("extra","receive");
x.t("solution","manual");
x.t("solution","overpayments");
x.t("solution","rock");
x.t("selected","just");
x.t("payment","just");
x.t("payment","check");
x.t("payment","receipt.");
x.t("payment","applies");
x.t("payment","receipt");
x.t("payment","already");
x.t("payment","transaction.");
x.t("notice","customer");
x.t("give","$200");
x.t("present.","click");
x.t("barley","$100.00.");
x.t("may","issue");
x.t("receive","payment");
x.t("instead","write");
x.t("cleaned","quirk");
x.t("cleaned","up:");
x.t("clearing","discounts");
x.t("memo","failure");
x.t("memo","created");
x.t("window.","create");
x.t("guess","just");
x.t("guess","guess");
x.t("return","$0.00");
x.t("issue","modification");
x.t("message","longer");
x.t("won\u2019t","show");
x.t("click","discount");
x.t("click","save");
x.t("click","done.");
x.t("wasn\u2019t","really");
x.t("longer","present.");
x.t("failure","resulted");
x.t("overpayments","refunds");
x.t("already","deposited");
x.t("already","selected");
x.t("actually","posts");
x.t("sees","refund");
x.t("$100.00.","expense");
x.t("wrote","earlier");
x.t("show","transaction");
x.t("overpayment","try");
x.t("overpayment","credit");
x.t("overpayment","manually.");
x.t("overpayment","portion");
x.t("correct","problem.");
x.t("answer.","doing");
x.t("$0.00","nothing");
x.t("$0.00","however");
x.t("have.","just");
x.t("save","close");
x.t("refund","check");
x.t("refund","overpayment");
x.t("manually.","example");
x.t("posts","just");
x.t("transaction","lists");
x.t("transaction","again");
x.t("transaction","$0.00");
x.t("transaction","adding");
x.t("note","quickbooks");
x.t("credits","available");
x.t("refunds","manual");
x.t("renee\u2019s","account");
x.t("quickbooks","business");
x.t("quickbooks","user");
x.t("quickbooks","confusion");
x.t("quickbooks","correctly");
x.t("mark","refund");
x.t("accountants","may");
x.t("correctly","sees");
x.t("resulted","problem.");
x.t("patch","quickbooks");
x.t("created","check");
x.t("created","earlier");
x.t("rock","castle");
x.t("adding","payment");
x.t("portion","gone.");
x.t("accounts","receivable");
x.t("transaction.","let\u2019s");
x.t("credits.","start");
x.t("done.","payment");
x.t("barley.","right");
x.t("even","importantly");
x.t("can\u2019t","delete");
x.t("renee","barley");
x.t("renee","barley.");
x.t("update","credit");
}
