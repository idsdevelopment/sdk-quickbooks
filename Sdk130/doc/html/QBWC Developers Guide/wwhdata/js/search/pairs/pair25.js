function FileData_Pairs(x)
{
x.t("pre-set","sdk");
x.t("active","discount");
x.t("active","inventory");
x.t("active","subtotal");
x.t("active","items");
x.t("active","vendors");
x.t("active","sales");
x.t("active","payment");
x.t("active","fixed");
x.t("active","customers");
x.t("active","non-inventory");
x.t("active","charge");
x.t("active","service");
x.t("active","accounts");
x.t("active","assembly");
x.t("active","employess");
x.t("email","contact");
x.t("email","employeeaddress");
x.t("available","single");
x.t("parameter","txndisplaymod");
x.t("parameter","listdisplaymod");
x.t("parameter","showing");
x.t("parameter","sessionid");
x.t("discount","items");
x.t("txndisplayadd","transaction");
x.t("balance","phone");
x.t("balance","totalbalance");
x.t("balance","vendoraddress/vendoraddressblock.");
x.t("description","accountquery");
x.t("customerquery","queries");
x.t("txndisplaymod","executes");
x.t("contact","fields");
x.t("itemfixedassetquery","queries");
x.t("intent","support");
x.t("user","input");
x.t("pager","pagerpin");
x.t("listdisplayadd","executes");
x.t("uses","specified");
x.t("support","during");
x.t("support","interactive");
x.t("mode","support");
x.t("provided","txnid");
x.t("provided","listid");
x.t("customer","customerquery");
x.t("inventory","items");
x.t("subtotal","items");
x.t("type","specified");
x.t("type","query");
x.t("listdisplaymod","executes");
x.t("response","user");
x.t("response","xml");
x.t("isusingtimedatatocreatepaychecks.","itemdiscountquery");
x.t("mobile","pager");
x.t("usetimedatatocreatepaychecks","isusingtimedatatocreatepaychecks.");
x.t("single","parameter");
x.t("items","itemfixedassetquery");
x.t("items","listdisplayadd");
x.t("items","itemservicequery");
x.t("items","itemsubtotalquery");
x.t("items","returning");
x.t("items","itempaymentquery");
x.t("items","iteminventoryquery");
x.t("items","itemquery");
x.t("items","itemgroupquery");
x.t("items","iteminventoryassemblyquery");
x.t("items","itemnoninventoryquery");
x.t("items","itemotherchargequery");
x.t("items","itemsalestaxquery");
x.t("itemservicequery","queries");
x.t("itemsubtotalquery","queries");
x.t("list","type");
x.t("list","display");
x.t("list","item");
x.t("doquery","uses");
x.t("doquery","operation");
x.t("doquery","invoke");
x.t("during","session");
x.t("group","items");
x.t("tax","items");
x.t("tax","group");
x.t("add","request");
x.t("itemdiscountquery","queries");
x.t("parameter.","txndisplayadd");
x.t("parameter.","vendoraddressquery");
x.t("directly","web");
x.t("specified","pre-set");
x.t("specified","type");
x.t("mod","request");
x.t("operation","request");
x.t("operation","present");
x.t("operation","offers");
x.t("name","printas");
x.t("name","fullname");
x.t("name","creditlimit");
x.t("name","fullname.");
x.t("vendors","returning");
x.t("communication","session");
x.t("returning","listid");
x.t("returning","elements");
x.t("pagerpin","email");
x.t("itempaymentquery","queries");
x.t("parameters","operation");
x.t("session","doquery");
x.t("session","web");
x.t("sales","tax");
x.t("sdk","queries");
x.t("sdk","requests");
x.t("web","connector");
x.t("web","service");
x.t("phone","mobile");
x.t("phone","fax");
x.t("fields","listed:");
x.t("fields","each.");
x.t("listed:","request");
x.t("billingaddress","customer");
x.t("iteminventoryquery","queries");
x.t("showing","list");
x.t("txnid","query");
x.t("input","syntax:");
x.t("request","description");
x.t("request","list");
x.t("request","parameters");
x.t("request","transaction");
x.t("present","case");
x.t("itemquery","queries");
x.t("queries","active");
x.t("queries","available");
x.t("queries","group");
x.t("queries","quickbooks.");
x.t("interactive","mode");
x.t("totalbalance","customer.");
x.t("itemgroupquery","queries");
x.t("payment","items");
x.t("display","add");
x.t("display","mod");
x.t("customer.","employeequery");
x.t("fixed","asset");
x.t("asset","items");
x.t("quickbooks.","intent");
x.t("quickbooks.","following");
x.t("listid","name");
x.t("listid","query");
x.t("requests","response");
x.t("requests","returning");
x.t("requests","interacting");
x.t("requests","using");
x.t("requests","invoke");
x.t("vendoraddress/vendoraddressblock.","vendorquery");
x.t("qbwc://","operation");
x.t("xml","except");
x.t("customers","returning");
x.t("printas","phone");
x.t("sublevel","balance");
x.t("connector","using");
x.t("iteminventoryassemblyquery","queries");
x.t("fullname","sublevel");
x.t("sessionid","guid");
x.t("guid","existing");
x.t("non-inventory","items");
x.t("creditlimit","balance");
x.t("existing","communication");
x.t("accountquery","queries");
x.t("specific","fields");
x.t("itemnoninventoryquery","queries");
x.t("charge","items");
x.t("vendoraddressquery","queries");
x.t("customerbilladdressquery","above");
x.t("query","parameter");
x.t("query","parameter.");
x.t("fax","email");
x.t("case","specify");
x.t("vendorquery","queries");
x.t("syntax:","qbwc://");
x.t("above","including");
x.t("itemotherchargequery","queries");
x.t("employeequery","queries");
x.t("fullname.","itemsalestaxgroupquery");
x.t("executes","list");
x.t("executes","transaction");
x.t("transaction","type");
x.t("transaction","display");
x.t("cases","specific");
x.t("interacting","directly");
x.t("following","requests");
x.t("following","preset");
x.t("service","items");
x.t("service","quickbooks.");
x.t("offers","following");
x.t("elements","response");
x.t("except","cases");
x.t("using","doquery");
x.t("itemsalestaxquery","queries");
x.t("preset","queries");
x.t("invoke","pre-set");
x.t("accounts","customerbilladdressquery");
x.t("assembly","items");
x.t("including","billingaddress");
x.t("employess","returning");
x.t("employeeaddress","usetimedatatocreatepaychecks");
x.t("specify","doquery");
x.t("itemsalestaxgroupquery","queries");
x.t("item","provided");
}
