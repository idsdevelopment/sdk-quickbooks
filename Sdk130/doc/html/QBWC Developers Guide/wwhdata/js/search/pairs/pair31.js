function FileData_Pairs(x)
{
x.t("example","web");
x.t("example","sendrequestxml");
x.t("indicated","error");
x.t("just","send");
x.t("just","called");
x.t("just","sent");
x.t("responded","sendrequestxml");
x.t("disrupted","unexpected");
x.t("don\u2019t","want");
x.t("error-recovery","capabilities");
x.t("current","session.");
x.t("them.","sent");
x.t("state","web");
x.t("state","handling");
x.t("state","information");
x.t("errors","web");
x.t("provided","sdk");
x.t("type","communication");
x.t("failure.","just");
x.t("failure.","communication");
x.t("authenticate","called");
x.t("certain","state");
x.t("want","blindly");
x.t("data-writing","requests");
x.t("indicates","type");
x.t("during","current");
x.t("expect","next");
x.t("getlasterror","web");
x.t("network","failure.");
x.t("guide","using");
x.t("maintain","certain");
x.t("communication","failure.");
x.t("communication","web");
x.t("determine","error-recovery");
x.t("web","connector");
x.t("web","service");
x.t("sdk","programmer\u2019s");
x.t("sdk","documented");
x.t("unexpected","state");
x.t("unexpected","network");
x.t("resend","them.");
x.t("error","responded");
x.t("queries","simply");
x.t("next","call");
x.t("know","whether");
x.t("whether","requests");
x.t("whether","failure");
x.t("instead","authenticate");
x.t("requests","wrote");
x.t("requests","again.");
x.t("requests","sent");
x.t("newmessagesetid","attributes");
x.t("handling","errors");
x.t("session.","example");
x.t("expected","call");
x.t("simply","resend");
x.t("connector","just");
x.t("connector","web");
x.t("programmer\u2019s","guide");
x.t("encounters","unexpected");
x.t("made","quickbooks");
x.t("occurred","happened");
x.t("sendrequestxml","web");
x.t("sendrequestxml","expected");
x.t("sendrequestxml","called");
x.t("failure","occurred");
x.t("send","data-writing");
x.t("actually","made");
x.t("sequence","occur");
x.t("wrote","data");
x.t("information","during");
x.t("data","quickbooks");
x.t("capabilities","provided");
x.t("documented","sdk");
x.t("called","indicates");
x.t("called","instead");
x.t("called","sendrequestxml");
x.t("described","document.");
x.t("blindly","just");
x.t("quickbooks","don\u2019t");
x.t("quickbooks","whether");
x.t("call","sequence");
x.t("call","either");
x.t("service","indicated");
x.t("service","disrupted");
x.t("service","expect");
x.t("service","maintain");
x.t("service","encounters");
x.t("either","receiveresponsexml");
x.t("using","oldmessagesetid");
x.t("receiveresponsexml","getlasterror");
x.t("occur","example");
x.t("happened","determine");
x.t("oldmessagesetid","newmessagesetid");
x.t("sent","queries");
x.t("sent","requests");
x.t("sent","actually");
x.t("again.","know");
x.t("attributes","described");
}
