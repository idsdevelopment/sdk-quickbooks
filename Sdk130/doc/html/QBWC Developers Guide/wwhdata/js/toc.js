function  WWHBookData_AddTOCEntries(P)
{
var A=P.fN("About This Guide","1");
var B=A.fN("Who Should Read This Guide","2");
B=A.fN("Before You Begin","3");
B=A.fN("What\u2019s New in This Release","4");
A=P.fN("Introduction to QBWC Programming","5");
B=A.fN("QuickBooks Supported by QBWC","6");
B=A.fN("Why Do I Need to Support QBWC in My Web Service?","7");
var C=B.fN("What is the COM Issue and How Does QBWC Solve This?","7#175605");
C=B.fN("What is the Firewall Issue and How Does QBWC Solve That?","7#175670");
B=A.fN("Are There Any Alternatives to QBWC?","8");
B=A.fN("The QBWC-to-Web Service Communication Universe","9");
C=B.fN("Initial Customer Interaction with Your Web Service","9#175905");
C=B.fN("Ongoing Communication Between QBWC and a Web Service","9#175899");
B=A.fN("What Will My Web Service Solution Look Like?","10");
C=B.fN("How to Build a QWC File","10#176528");
C=B.fN("How to Build QBWC Support into Your Web Service","10#176506");
B=A.fN("Are There Samples to Jumpstart My Work?","11");
B=A.fN("Frequently Asked Questions","12");
C=B.fN("What Platforms and Languages can I use in my Implementation?","12#176004");
C=B.fN("Which QuickBooks/QB POS Versions Support QBWC?","12#176189");
C=B.fN("Can I Specify Which QuickBooks Editions Access My Service?","12#176199");
C=B.fN("Does My Web Service Need a Certificate to Access QBWC?","12#176292");
C=B.fN("Where is the QBWC WSDL?","12#175361");
C=B.fN("Is There a Limit to the Number of Messages I Send to QBWC?","12#175262");
C=B.fN("Why QBWC and Not a Simple Web Interface?","12#175204");
A=P.fN("The QBWC Communication Model","13");
B=A.fN("A Closer Look at the Communication Model","14");
A=P.fN("Implementing a Web Service for QBWC","15");
B=A.fN("","");
C=B.fN("Generating and Implementing the Service Skeleton with .NET","15#168688");
C=B.fN("Generating and Implementing the Service Skeleton with Java and Apache Axis","15#168717");
A=P.fN("Building The QWC File for Your Users","16");
B=A.fN("A Sample QWC File","17");
C=B.fN("How Do I Set the QBWCXML Fields in the QWC File?","17#171869");
B=A.fN("How Does the User Add the QWC File?","18");
A=P.fN("Exchanging Data with QuickBooks and QBPOS","19");
B=A.fN("A Note About the Required NameSpace","20");
B=A.fN("Data Exchange Considerations","21");
A=P.fN("Interacting Directly with the Web Connector","22");
B=A.fN("How to Implement Interactive Mode","23");
B=A.fN("Using docontrol to Change Web Service Behavior in QBWC","24");
C=B.fN("Sample docontrol URLs","24#167662");
B=A.fN("Using doquery to Invoke Pre-Set SDK Requests","25");
A=P.fN("Understanding the End-User Experience and Setup","26");
B=A.fN("Initial End User Setup","27");
A=P.fN("Handling Errors","28");
B=A.fN("The Web Connector Cannot Access QuickBooks","29");
C=B.fN("How Your Web Service Should Respond to QB Access Errors","29#169293");
B=A.fN("The Web Service Gets Unexpected Data from Web Connector","30");
C=B.fN("How Your Web Service Should Respond to Unexpected Data","30#169475");
B=A.fN("The Web Service Encounters an Unexpected State","31");
A=P.fN("How Do I TroubleShoot Problems?","32");
B=A.fN("About Logging","33");
B=A.fN("How Do I Get to the Troubleshooting Page?","34");
B=A.fN("What Is Provided at the Troubleshooting Pages?","35");
A=P.fN("QBWC Callback Web Method Reference","36");
B=A.fN("authenticate","37");
B=A.fN("clientVersion","38");
B=A.fN("closeConnection","39");
B=A.fN("connectionError","40");
B=A.fN("getInteractiveURL","41");
B=A.fN("getLastError","42");
B=A.fN("getServerVersion","43");
B=A.fN("interactiveDone","44");
B=A.fN("interactiveRejected","45");
B=A.fN("receiveResponseXML","46");
B=A.fN("sendRequestXML","47");
A=P.fN("Understanding and Responding to QBWC Error Codes","48");
}
