// RequestProcessor.cpp : Wrapper of CRequestProcessor
#include "stdafx.h"
#include "qbXMLRP2e.h" 
#include "RequestProcessor.h"

/////////////////////////////////////////////////////////////////////////////
// CRequestProcessor


STDMETHODIMP CRequestProcessor::VerifyQBXMLRP()
{ 
	if( qbXMLRPPtr == NULL )
	{
		HRESULT hr = qbXMLRPPtr.CreateInstance(__uuidof (QBXMLRP2Lib::RequestProcessor2));
		return hr;
	}
	return S_OK;
}


STDMETHODIMP CRequestProcessor::OpenConnection(BSTR appID, BSTR appName)
{
	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	return qbXMLRPPtr->raw_OpenConnection( appID, appName);
}

STDMETHODIMP CRequestProcessor::ProcessRequest(BSTR ticket, BSTR inputRequest, BSTR *outputResponse)
{
	if (inputRequest == NULL) 
	{
		if (outputResponse != NULL) {
			*outputResponse = NULL;
		}
		return S_OK;
	}

	if (outputResponse == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	return qbXMLRPPtr->raw_ProcessRequest(ticket, inputRequest, outputResponse);
}

STDMETHODIMP CRequestProcessor::CloseConnection()
{
	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	hr = qbXMLRPPtr->raw_CloseConnection();
	qbXMLRPPtr = NULL;
	return hr;
}

STDMETHODIMP CRequestProcessor::BeginSession(BSTR qbFileName, QBFileModeE reqFileMode, BSTR *pTicket)
{
	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	hr =  qbXMLRPPtr->raw_BeginSession(qbFileName, (QBXMLRP2Lib::QBFileMode)reqFileMode, pTicket);
	return hr;
}

STDMETHODIMP CRequestProcessor::EndSession(BSTR ticket)
{
	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	return qbXMLRPPtr->raw_EndSession(ticket);
}

STDMETHODIMP CRequestProcessor::GetCurrentCompanyFileName(BSTR ticket, BSTR *pFileName)
{
	if (pFileName == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	return qbXMLRPPtr->raw_GetCurrentCompanyFileName(ticket, pFileName);
}

STDMETHODIMP CRequestProcessor::get_MajorVersion(short *pVal)
{
	if (pVal == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	hr = qbXMLRPPtr->get_MajorVersion(pVal);
	return hr;
}

STDMETHODIMP CRequestProcessor::get_MinorVersion(short *pVal)
{
	if (pVal == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	hr = qbXMLRPPtr->get_MinorVersion(pVal);
	return hr;
}

STDMETHODIMP CRequestProcessor::get_ReleaseLevel(QBXMLRPReleaseLevelE *pVal)
{
	if (pVal == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	QBXMLRP2Lib::QBXMLRPReleaseLevel level;
	hr = qbXMLRPPtr->get_ReleaseLevel(&level);
	if( SUCCEEDED( hr ) )
	{
		*pVal = (QBXMLRPReleaseLevelE)level;
	}
	return hr;
}

STDMETHODIMP CRequestProcessor::get_ReleaseNumber(short *pVal)
{
	if (pVal == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	hr = qbXMLRPPtr->get_ReleaseNumber(pVal);
	return hr;
}

STDMETHODIMP CRequestProcessor::get_QBXMLVersionsForSession(BSTR ticket, SAFEARRAY **pVal)
{
	if (pVal == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	hr = qbXMLRPPtr->get_QBXMLVersionsForSession(ticket, pVal);
	return hr;
}

STDMETHODIMP CRequestProcessor::get_ConnectionType(QBXMLRPConnectionTypeE *pVal)
{
	if (pVal == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	QBXMLRP2Lib::QBXMLRPConnectionType connectionType;
	hr = qbXMLRPPtr->get_ConnectionType(&connectionType);
	if( SUCCEEDED( hr ) )
	{
		*pVal = (QBXMLRPConnectionTypeE)connectionType;
	}
	return hr;
}

STDMETHODIMP CRequestProcessor::ProcessSubscription(BSTR inputRequest, BSTR *outputResponse)
{
	if (inputRequest == NULL) 
	{
		if (outputResponse != NULL) {
			*outputResponse = NULL;
		}
		return S_OK;
	}

	if (outputResponse == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	return qbXMLRPPtr->raw_ProcessSubscription(inputRequest, outputResponse);
}

STDMETHODIMP CRequestProcessor::get_QBXMLVersionsForSubscription(SAFEARRAY **ppsa)
{
	if (ppsa == NULL) {
		return E_POINTER;
	} 

	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	hr = qbXMLRPPtr->get_QBXMLVersionsForSubscription(ppsa);
	return hr;
}

STDMETHODIMP CRequestProcessor::OpenConnection2(BSTR appID, BSTR appName, QBXMLRPConnectionTypeE connPref)
{
	HRESULT hr = VerifyQBXMLRP();
	if  ( FAILED(hr) )
	{
		return hr;
	}
	return qbXMLRPPtr->raw_OpenConnection2( appID, appName, (QBXMLRP2Lib::QBXMLRPConnectionType)connPref);
}

STDMETHODIMP CRequestProcessor::get_AuthPreferences(IAuthPreferencesE** ppAuthPreferences) {
	if (ppAuthPreferences == NULL) {
		return E_POINTER;
	} 

	// if it already exists, use it
	if (authPrefsEPtr != NULL) {
		authPrefsEPtr.p->AddRef();
		*ppAuthPreferences = authPrefsEPtr.p;
		return S_OK;
	}

	// it doesn't exist yet, so create it
	HRESULT hr = CAuthPreferencesE::CreateInstance(&authPrefsEPtr);
	if( FAILED ( hr ) ) {
		authPrefsEPtr = NULL;
		return hr;
	}

	// we'll need to cast it for a method call
	// unable to use the following dynamic cast due to warning C4541
	// CAuthPreferencesE* castedAuthPrefsEPtr = dynamic_cast<CAuthPreferencesE*>(authPrefsEPtr.p);
	CAuthPreferencesE* castedAuthPrefsEPtr = (CAuthPreferencesE*)(authPrefsEPtr.p);
	if (!castedAuthPrefsEPtr) {
		authPrefsEPtr = NULL;
		return E_FAIL;	//if we'd used a dynamic cast, this would've been E_DYNAMIC_CAST
	}

	// get the pointer to qbXMLRP
	hr = VerifyQBXMLRP();
	if( FAILED( hr ) ) {
		authPrefsEPtr = NULL;
		return hr;
	}

	QBXMLRP2Lib::IAuthPreferencesPtr realAuthPrefsPtr;

	// get the "real" AuthPreferences pointer (for qbXMLRP)
	hr = qbXMLRPPtr->get_AuthPreferences(&realAuthPrefsPtr);
	if( FAILED ( hr ) ) {
		authPrefsEPtr = NULL;
		return hr;
	}
	
	// and put it in our AuthPreferencesE wrapper
	castedAuthPrefsEPtr->SetAuthPreferences(realAuthPrefsPtr);

	// return the explicit pointer to our AuthPreferencesE
	*ppAuthPreferences = authPrefsEPtr.p;

	return S_OK;
}
