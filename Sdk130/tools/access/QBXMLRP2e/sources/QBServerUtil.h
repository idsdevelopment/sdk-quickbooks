// QBServerUtil.h: interface for the QBServerUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_QBSERVERUTIL_H__809BEA1A_4FC1_46E2_9982_5D371BA061C8__INCLUDED_)
#define AFX_QBSERVERUTIL_H__809BEA1A_4FC1_46E2_9982_5D371BA061C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CQBServerUtil  
{
public:
	static BOOL DeleteRegValue(HKEY mainKey, PTCHAR keyPath, PTCHAR keyName);
	static BOOL SetRegValue(HKEY mainKey, PTCHAR keyPath, PTCHAR keyName, LPCTSTR value);
	static BOOL IsRegValueExist(HKEY mainKey, PTCHAR keyPath, PTCHAR keyName);
	static BOOL Register9xService(BOOL bRegister);
	static const bool IsNT();
	CQBServerUtil();
	virtual ~CQBServerUtil();

};

#endif // !defined(AFX_QBSERVERUTIL_H__809BEA1A_4FC1_46E2_9982_5D371BA061C8__INCLUDED_)
