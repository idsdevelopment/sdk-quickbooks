Attribute VB_Name = "qbooks"
'
' Copyright � 2002 Intuit Inc. All rights reserved.
' Use is subject to the terms specified at:
'      http://developer.intuit.com/legal/devsite_tos.html
'
Option Explicit
'
' Deliver the qbXML request to QuickBooks and return the
' response.
'
Public Function post(xmlStream As String) As String

    On Error GoTo errHandler
    
' The only lines of code changed in the file to do DCOM,
' is to replace the following line of code
    ' Open connection to qbXMLRP COM
    'Dim qbXMLRP As New QBXMLRPLib.RequestProcessor
' with these two lines of code.
    'Create a copy of the qbxmlrp wrapper.
    'The wrapper can be created on this computer or another computer
    Dim qbXMLRP As QBXMLRPELib.RequestProcessor
    Set qbXMLRP = CreateObjectEx("QBXMLRPe.RequestProcessor.1", UIForm.ComputerName.Text)
    
    qbXMLRP.OpenConnection "", App.Title
        
    ' Begin Session
    ' Pass empty string for the data file name to use the currently
    ' open data file.
    Dim ticket  As String
    ticket = qbXMLRP.BeginSession("", QBXMLRPELib.qbFileOpenDoNotCare)
        
    ' Send request to QuickBooks
    post = qbXMLRP.ProcessRequest(ticket, xmlStream)
    
    ' End the session
    qbXMLRP.EndSession ticket
    ticket = ""
    
    ' Close the connection
    qbXMLRP.CloseConnection
    Exit Function
    
errHandler:
    post = ""
    Log.out ("Error in post" & vbCrLf & "HRESULT = " & Err.Number & " (" & Hex(Err.Number) & ") " & vbCrLf & Err.Description)
    If (Not qbXMLRP Is Nothing) Then
        If (ticket <> "") Then
            ' End the session
            qbXMLRP.EndSession ticket
        End If
        
        ' Close the connection
        qbXMLRP.CloseConnection
    End If
End Function
