
This sample is a simple ASP.NET web form application that adds a customer to Quickbooks.
QuickBooks and .NET must be installed on the machine. The sample uses VB.NET language.

The sample shows how to mix QBFC and qbXMLRP2 calls. QBFC is used to build the qbXML request 
and parse the qbXML response.

Because of IIS4 and 5 threading and security model, an ASP.NET page cannot access directly 
qbXMLRP2.dll because its BeginSession fails. To solve this problem, an EXE COM wrapper around qbXMLRP2.dll
was created (qbXMLRP2e) to insure the right configuration. This wrapper is configured to run
as the interactive user, thus insuring that it shares the same active desktop as QuickBooks.

Building/Installing the sample
-------------------
Copy the entire CustomerAdd sample directory under Inetpub\wwwroot. 
Make sure that the Inetpub\wwwroot\CustomerAdd\bin files are not read-only.
Open Inetpub\wwwroot\CustomerAdd\CustomerAdd.sln in Microsoft Visual Studio .NET and build the solution.

Before running, make sure that you register the qbXMLRPe.exe wrapper and that the 
CustomerAdd IIS virtual directory local path points to Inetpub\wwwroot\CustomerAdd. 
The fast way to do this last step is to:
-open Internet Services Manager (under Control Panel->Administrative Tools for Win2000)
-right mouse click, and select Properties for 'CustomerAdd' under Default Web Site
-on the CustomerAdd Properties->Directory tab, 
click on 'Create' button (which changes right away into 'Remove'), then click OK.

Running the sample
------------------

Before running CustomerAdd.exe, make sure that .NET runtime is installed on the machine, 
and QuickBooks is running with a company opened. 

Register the COM qbXMLRP2e.exe wrapper, by running 'qbXMLRP2e.exe /RegServer'

Copy the entire CustomerAdd sample directory under Inetpub\wwwroot. 
Create CustomerAdd IIS virtual directory for this folder. The fast way to do this last step is to:
-open Internet Services Manager (under Control Panel->Administrative Tools for Win2000)
-right mouse click, and select Properties for 'CustomerAdd' under Default Web Site
-on the CustomerAdd Properties->Directory tab, 
click on 'Create' button (which changes right away into 'Remove'), then click OK.

Give the ASPNET account permissions to launch and access qbXMLRP2e COM object using dcomcnfg 
by doing the following:
1. Launch DCOMCNFG by clicking the Start button, selecting Run, and typing "Dcomcnfg" in the Run dialog box
2. Select qbXMLRP2e and click 'Properties...' button  (on Windows XP you will need to navigate the following
   path in the management console:  
         Console Root->Component Services->Computers->My Computer->DCOM Components->qbXMLRP2e)
3. In the Security tab select 'Use Custom access permissions' and click Edit, 
   then add ASPNET user and INTERACTIVE to the Registry Value Permissions dialog box
   Note that IIS must be installed before the .NET framework in order for the ASPNET account to be
   created.
4. Repeat step 3 for the 'Use Custom launch permissions' button.

Open a browser and point it to 
http://localhost/CustomerAdd/CustomerAddWebForm.aspx
