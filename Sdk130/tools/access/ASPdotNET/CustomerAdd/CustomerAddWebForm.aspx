<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CustomerAddWebForm.aspx.vb" Inherits="CustomerAdd.CustomerAddWebForm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:TextBox id="Name" style="Z-INDEX: 101; LEFT: 140px; POSITION: absolute; TOP: 42px" runat="server" Width="274px" Height="22px"></asp:TextBox>
			<asp:Label id="Label1" style="Z-INDEX: 102; LEFT: 29px; POSITION: absolute; TOP: 45px" runat="server" Width="101px" Height="21px">Customer Name</asp:Label>
			<asp:Button id="CustomerAdd" style="Z-INDEX: 103; LEFT: 309px; POSITION: absolute; TOP: 69px" runat="server" Width="105px" Height="26px" Text="Add Customer"></asp:Button>
			<asp:Label id="XMLResponse" style="Z-INDEX: 104; LEFT: 28px; POSITION: absolute; TOP: 118px" runat="server" Width="385px" Height="191px" ForeColor="Navy"></asp:Label>
			<asp:Label id="Label2" style="Z-INDEX: 105; LEFT: 30px; POSITION: absolute; TOP: 78px" runat="server" Height="10px" Width="249px" Font-Size="8pt">Please make sure QuickBooks is running before clicking Add Customer button.</asp:Label>
		</form>
	</body>
</HTML>
