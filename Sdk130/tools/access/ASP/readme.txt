
This sample is a simple ASP page that queries for all the customers from Quickbooks. 

Because of IIS4 and 5 threading and security model, an ASP page cannot access directly 
qbXMLRP.dll because its BeginSession fails. To solve this problem, an EXE COM wrapper around qbXMLRP.dll
was created (qbXMLRPe) to insure the right configuration. This wrapper is configured to run
as the interactive user, thus insuring that it shares the same active desktop as QuickBooks.

Running the sample
------------------

Before running CustomerAdd.exe, make sure that QuickBooks is running with a company opened. 

Register the COM qbXMLRPe.exe wrapper, by running qbXMLRPe\qbXMLRPe.exe /RegServer

Copy testQB.asp under Inetpub\wwwroot.

Give the IUSR_<machine_name> account permissions to launch and access qbXMLRPe COM object using dcomcnfg 
by doing the following:
1. Lauch DCOMCNFG by clicking the Start buttom, selecting Run, and typig "Dcomcnfg" in the Run dialog box
2. Select qbXMLRPe and click 'Properties...' button
3. In the Security tab select 'Use Custom access permissions' and click Edit, 
then add IUSR_<machine_name> user and INTERACTIVE to the Registry Value Permiossions dialog box
4. Repeat similar step for the 'Use Custom launch permissions'.

Open a browser and point it to 
http://localhost/testQB.asp

