<HTML>
<HEAD>
<TITLE>Customer Query</TITLE>
</HEAD>

<BODY >

<%
Sub DoWork
	On Error Resume Next

	'step 1: create QBXMLRP2E object (EXE wrapper around QBXMLRP)
	Set rp = Server.CreateObject("QBXMLRP2E.RequestProcessor")
	if( Err.Number <> 0 ) then
		Response.Write(" <BR> Cannot createObject QBXMLRP2e.RequestProcessor: " & Err.Description)
		Exit Sub
	end if

	'step2: Connect to QB
	rp.OpenConnection "ASP-s", "ASP sample"
	if( Err.Number <> 0 ) then
		Response.Write(" <BR> OpenConnection failed: " & Err.Description)
		Exit Sub
	end if

	ticket = rp.BeginSession( "", 0 )
	if( Err.Number <> 0 ) then
		Response.Write(" <BR> BeginSession failed (make sure QuickBooks is running and access is granted to this application): " & Err.Description)
		rp.CloseConnection
		Exit Sub
	end if

	'step3: send request
	inRq = 		"<?xml version=""1.0"" ?>"
	inRq = inRq &   "<?qbxml version=""2.0""?>"
	inRq = inRq &	"<QBXML>"
	inRq = inRq &	"<QBXMLMsgsRq onError = ""continueOnError"">"
	inRq = inRq &	"<CustomerQueryRq requestID = ""0""/>"
	inRq = inRq &	"</QBXMLMsgsRq>"
	inRq = inRq &	"</QBXML>"

	outRs = rp.ProcessRequest(ticket, inRq)
	if( Err.Number <> 0 ) then
		Response.Write(" <BR/> ProcessReques failed: " & Err.Description)
	else
		Response.Write(" <BR/> Customer Query Response: <p/> <TEXTAREA rows=""20"" cols=""80"">" & outRs & "</TEXTAREA" )
	end if

	rp.EndSession ticket
	rp.CloseConnection

End Sub

'call it
DoWork
%>


</BODY>
</HTML>


